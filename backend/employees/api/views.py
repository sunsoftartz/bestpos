from rest_framework.views import APIView
from django.db.models import Q
from rest_framework.generics import *
from rest_framework.response import Response
#from rest_framework.mixins import *
from rest_framework.permissions import *
from rest_framework_jwt.settings import api_settings
from .serializers import *

from django.contrib.auth import get_user_model, authenticate

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER


User = get_user_model()

class AuthView(APIView):
    authentication_classes = []
    permission_classes     = [AllowAny]
    serializer_class       = UserSerializer 

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return Response({'detail': 'You are already authenticated'}, status=400)
        data = request.data
        username = data.get('username')
        password = data.get('password')
        user = authenticate(username=username,password=password)
        qs = User.objects.filter(
            Q(username__iexact=username)|
            Q(email__iexact=username)
        ).distinct()
        if qs == 1:
            user_obj = qs.first()
            if user_obj.check_password(password):
                user = user_obj
                payload =jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                response = jwt_response_payload_handler( token, user, request=request)        
                return Response(response)
        return Response({'detail': 'Invalid Credentials'}, status=401)

class UserRegisterView(CreateAPIView):
    qs = User.objects.all()
    serializer_class = UserRegisterSerializer
    permission_classes = [AllowAny]

""" class CreateUserView(CreateAPIView):
    serializer_class = UserSerializerWithToken
    permission_classes = (AllowAny)


class ManageUserView(RetrieveUpdateAPIView):
    serializer_class = UserDetailSerializer
    permission_classes = (IsAuthenticated)

    def get_object(self):
        return self.request.user """   