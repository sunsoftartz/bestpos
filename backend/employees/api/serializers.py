from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from django.contrib.auth import get_user_model
import datetime
from django.utils import timezone

jwt_payload_handler          = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler           = api_settings.JWT_ENCODE_HANDLER
jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER
expire_delta                 = api_settings.JWT_REFRESH_EXPIRATION_DELTA

User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'password',
        ]

class UserRegisterSerializer(serializers.ModelSerializer):
    #password  = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    token     = serializers.SerializerMethodField('get_token')
    expires   = serializers.SerializerMethodField('get_expires')    
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            'password2',
            'token',
            'expires',
        ]
        #extra_kwargs = {'password': {'write_only=True'}}

    def get_expires(self, obj):
        return timezone.now() + expire_delta - datetime.timedelta(seconds=200)

    def validate_email(self, value):
        qs = User.objects.filter(email__iexact=value)
        if qs.exists():
            raise serializers.ValidationError('User with this email already exists')
        return value

    def validate_username(self, value):
        qs = User.objects.filter(username__iexact=value)
        if qs.exists():
            raise serializers.ValidationError('User with this username already exists')
        return value

    def get_token(self, obj): #instance of the model used
        user = obj
        payload = jwt_payload_handler(user)
        token   = jwt_encode_handler(payload)
        return token

    def validate(self,data):
        pw  = data.get('password')
        pw2 = data.pop('password2')
        if pw != pw2:
            raise serializers.ValidationError("Passwords must match.")
        return data
            
    def create(self, validated_data):
        user_obj = User(
            username=validated_data.get('username'),
            email=validated_data.get('email'))
        user_obj.set_password(validated_data.get('password'))
        user_obj.save()
        return user_obj
            
        

""" class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User()        
        fields = ('id', 'username', 'is_superuser', 'first_name', 'last_name')

class UserSerializerWithToken(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)
    token = serializers.SerializerMethodField()

    def get_token(self, object):

        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings

        payload = jwt_payload_handler(object)
        token = jwt_encode_handler(payload)

        return token

    def create(self, validated_data):
        user = User().objects.create(
            username = validated_data['username'],
            first_name = validated_data['first_name'],
            last_name = validated_data['last_name'],
        )
        user.set_password(validated_data['password']), 
        user.save()
        return user        
          
    class Meta:
        model = User()
        fields = ('token', 'username', 'password', 'first_name', 'last_name')
 """