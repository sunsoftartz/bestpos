from django.db import models
from inventory.models import Products

class CartItem(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    tax = models.FloatField(default=0.00)

    objects = models.Manager()

    def __str__(self):
        return str(self.product)      

        
class Cart(models.Model):    
    item = models.ManyToManyField(CartItem)
       

    objects = models.Manager()

    def __str__(self):
        return str(self.item)

    def update_quantity(self, item):
        pass
        
    


        