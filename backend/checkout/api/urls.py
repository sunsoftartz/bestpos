from django.urls import path, include

from .views import *

urlpatterns = [
    path('cart-item/', CartItemView.as_view()),
    path('cart-item/<pk>',CartItemDetailView.as_view()),
    path('cart/', CartView.as_view()),
    path('cart/<pk>', CartDetailView.as_view()),
]