from rest_framework.views import APIView
from django.db.models import Q
from rest_framework.generics import *
from rest_framework.response import Response
from rest_framework.mixins import *
from rest_framework.permissions import *
from .serializers import *

from checkout.models import (
    Cart as CartModel,
    CartItem as CartItemModel)

class CartItemView(CreateModelMixin,ListAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = CartItemSerializer
    
    def get_queryset(self):
        qs = CartItemModel.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(content__icontains=query)
        return qs

    def post(self, request, *args, **kwargs):
        return self.create (request, *args, **kwargs)

class CartItemDetailView(UpdateModelMixin, DestroyModelMixin, RetrieveAPIView):
    permission_classes = []
    authetication_classes = []
    serializer_class = CartItemSerializer
    queryset = CartItemModel.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class CartView(CreateModelMixin,ListAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = CartSerializer
    
    def get_queryset(self):
        qs = CartModel.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(content__icontains=query)
        return qs

    def post(self, request, *args, **kwargs):
        return self.create (request, *args, **kwargs)

class CartDetailView(UpdateModelMixin, DestroyModelMixin, RetrieveAPIView):
    permission_classes = []
    authetication_classes = []
    serializer_class = CartSerializer
    queryset = CartModel.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)