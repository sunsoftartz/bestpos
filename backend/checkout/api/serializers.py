from rest_framework import serializers
from checkout.models import Cart, CartItem
from inventory.models import Products

class CartItemSerializer(serializers.ModelSerializer):
    product_code = serializers.IntegerField(source='product.code')
    product_price= serializers.DecimalField(decimal_places=2, max_digits=100,source='get_product_price')

    class Meta:
        model = CartItem
        fields = [
            'product_code',
            'product',
            'product_price',
            'quantity',
            'tax',
        ]
        
    
    def get_code(self, obj):
        return obj.code
        
    def get_product_price(self, obj):
        return obj.sale_price

class CartSerializer(serializers.ModelSerializer):
    code = serializers.Field(source='item.code')
    price= serializers.Field(source='item.price')
    quantity = serializers.Field(source='item.quantity')
    extended_price = serializers.SerializerMethodField('calculate_extended_price')
    calculated_tax = serializers.SerializerMethodField('calculate_tax')
    subtotal = serializers.SerializerMethodField('calculate_subtotal')
    total_tax = serializers.SerializerMethodField('calculate_total_tax')
    total = serializers.SerializerMethodField('calculate_total')

    class Meta:
        model = Cart
        fields = [
            'code',
            'item',
            'price',
            'quantity',
            'extended_price',
            'calculated_tax',
            'subtotal',
            'total_tax',
            'total'
        ]
        depth = 1
        
    
    def calculate_extended_price(self, obj):        
        pass

    def calculate_tax(self, value):
        pass

    def calculate_subtotal(self, value):
        pass

    def calculate_total_tax(self,value):
        pass    
    
    def calculate_total(self, value):
        pass