from django.contrib import admin
from .models import Products, Supplier

admin.site.register(Products)
admin.site.register(Supplier)