from django.urls import path, include

from .views import *

urlpatterns = [
    path('products', Products.as_view()),
    path('products/<pk>', ProductsDetail.as_view()),
    path('supplier', SupplierView.as_view()),
    path('supplier/<pk>', SupplierDetailView.as_view()),
]