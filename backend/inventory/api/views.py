from rest_framework.views import APIView
from rest_framework.generics import *
from rest_framework.response import Response
from rest_framework.mixins import *
from rest_framework.permissions import *
from rest_framework.parsers import FileUploadParser

from .serializers import *
from inventory.models import Products as ProductsModel, Supplier as SupplierModel
  
  
class ProductsListSearch(APIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = ProductsSerializer

    def get(self, request, format=None):
        qs = ProductsModel.objects.all()
        serializer = ProductsSerializer(qs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        qs =Products.objects.all()
        serializer = ProductsSerializer(qs, many=True)
        return Response(serializer.data)

class Products(CreateModelMixin,ListAPIView):
    parser_class = (FileUploadParser)
    permission_classes = []
    authentication_classes = []
    serializer_class = ProductsSerializer
    
    def get_queryset(self):
        qs = ProductsModel.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(content__icontains=query)
        return qs

    def post(self, request, *args, **kwargs):
        return self.create (request, *args, **kwargs)


#class CreateProducts(CreateAPIView):
    #permission_classes = []
    #authetication_classes = []
    #serializer_class = ProductsSerializer
    
    #def perform_create(self, serializer):
        #serializer.save(product=self.request.products)       

class ProductsDetail(UpdateModelMixin, DestroyModelMixin, RetrieveAPIView):
    parser_class = (FileUploadParser)
    permission_classes = []
    authetication_classes = []
    serializer_class = ProductsSerializer
    queryset = ProductsModel.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

#class UpdateProducts(UpdateAPIView):
    #permission_classes = []
    #authentication_classes = []
    #serializer_class = ProductsSerializer

#class DeleteProducts(DestroyAPIView):
    #permission_classes = []
    #authentication_classes = []
    #serializer_class = ProductsSerializer

class SupplierView(CreateModelMixin,ListAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = SupplierSerializer
    
    def get_queryset(self):
        qs = SupplierModel.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(content__icontains=query)
        return qs

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class SupplierDetailView(UpdateModelMixin, DestroyModelMixin, RetrieveAPIView):
    permission_classes = []
    authetication_classes = []
    serializer_class = SupplierSerializer
    queryset = SupplierModel.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)