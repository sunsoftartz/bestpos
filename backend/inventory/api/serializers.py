from rest_framework import serializers
from inventory.models import Products, Supplier

class ProductsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Products
        fields = [
            'code',
            'name',
            'description',
            'image',
            'purchase_price',
            'sale_price',
            'quantity',
            'tax',
        ]

class SupplierSerializer(serializers.ModelSerializer):

    class Meta:
        model = Supplier
        fields = [
            'name',
            'address',
            'city',
            'email',
            'contact',
            'product',
        ]
