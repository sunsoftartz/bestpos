import os
from django.db import models
from django.contrib.auth.models import User

#def get_upload_to(self, instance, filename):
        #folder_name = 'images'
        #filename = self.file.field.storage.get_valid_name(filename)
        #return os.path.join(folder_name, filename)

class ProductsQueryset(models.QuerySet):
    def get_queryset(self):
        return self

class ProductsManager(models.Manager):
    def get_queryset(self):
        return ProductsQueryset(self.model, using=self._db)

class Products(models.Model):
    code = models.IntegerField(unique=True,blank=True,default=0)
    name = models.CharField(max_length=200, null=True)
    description = models.CharField(max_length=200,null=True)
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True)
    sale_price = models.DecimalField(default=0.00,decimal_places=2,max_digits=100)
    quantity = models.IntegerField(default=0)
    tax = models.BooleanField(default=False)

    objects = models.Manager()

    def __str__(self):
        return self.name

class Supplier(models.Model):
    name = models.CharField(unique=True, max_length=100)
    address = models.CharField(max_length=100, blank=False)
    city = models.CharField(max_length=100,blank=False)
    email = models.EmailField(blank=True)
    contact = models.IntegerField(null=True, blank=True)
    product = models.ManyToManyField(Products)

    objects = models.Manager()

    def __str__(self):
        return self.name
