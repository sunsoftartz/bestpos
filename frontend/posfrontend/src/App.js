import React, {Component} from 'react';
import CheckoutPage from './pages/CheckoutPage/CheckoutPage';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <CheckoutPage/>
      </div>
    );
  }
}
export default App;
